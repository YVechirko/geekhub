<?php
function hanoi($n, $a, $c, $b){
    if($n==1){
      echo $a,"->",$c," <br>";
    } else {
      hanoi($n-1,$a,$b,$c);
      echo $a,"->",$c,"<br> ";
      hanoi($n-1,$b,$c,$a);
    }
}
hanoi(4, 'A', 'C', 'B');