<?php
/**
 * php Implementation of queue with array: FIFO
    1. getLength(): Get the length of the queue
    2. isEmpty(): Judge whether the queue is empty
    3. enqueue(): Join the team and add data at the end of the team.
    4. dequeue(): Leave the team, return and remove the team head data. You can't leave the team when it's empty.
    5. show(): Traverse the queue and output
    6. clear(): Clear queue
 */
class Queue {
    // Queue array
    public $dataStore = [];

    // Get the length of the queue
    public function getLength() {
        return count($this->dataStore);
    }
    // Judge whether the queue is empty
    public function isEmpty() {
        return $this->getLength() === 0;
    }
    // Join the team and add data at the end of the team.
    public function enqueue($element) {
        $this->dataStore[] = $element;
        // array_push($this->dataStore, $element);
    }
    // Leave the team, return and remove the team head data. You can't leave the team when it's empty.
    public function dequeue() {
        if (!$this->isEmpty()) {
            return array_shift($this->dataStore);
        }
        return false;
    }
    // Traverse the queue and output
    public function show() {
        if (!$this->isEmpty()) {
            for ($i = 0; $i < $this->getLength(); $i++) {
                echo $this->dataStore[$i] . PHP_EOL;
            }
        } else {
            return "empty";
        }
    }
    // Clear queue
    public function clearQueue() {
        unset($this->dataStore);
        // $this->dataStore = array();
    }
}

$queue = new Queue();
$queue->enqueue('1');
$queue->enqueue('2');
$queue->enqueue('3');
echo $queue->getLength()."<br>";
$queue->show().'<br>';
$queue->dequeue()."<br>";
$queue->show();