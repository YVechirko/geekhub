<?php
/**
 * php Implement stack with array: LIFO
    1. getLength(): Get the length of the stack
    2. push(): In the stack, add data at the top level.
    3. pop(): Stack, return and remove the top-level data.
    4. getTop(): Returns the value of the topmost data, but does not remove it
    5. clearStack(): Empty stack
    6. show(): Traversal stack element
 */
class Stack {
    // Using array to realize stack structure
    public $stack = [];

    // Get the length of the stack
    public function getLength() {
        return count($this->stack);
    }
    // In the stack, add data at the top level.
    public function push($element) {
        $this->stack[] = $element;
    }
    // Stack, return and remove the top-level data.
    public function pop() {
        if ($this->getLength() > 0) {
            return array_pop($this->stack);
        }
    }
    // Returns the value of the topmost data, but does not remove it
    public function getTop() {
        $top = $this->getLength() - 1;
        return $this->stack[$top];
    }
    // Empty stack
    public function clearStack() {
        unset($this->stack);
        // $this->stack = array();
    }
    // Traversal stack element
    public function show() {
        if ($this->getLength() > 0) {
            for ($i = 0; $i < $this->getLength(); $i++) {
                echo $this->stack[$i] . PHP_EOL;
            }
        }
        echo "Empty!";
    }
}

$s = new Stack();
$s->push(1);
$s->push(2);
$s->push(3);
echo $s->getLength()."<br>";
$s->pop();
echo $s->getLength();