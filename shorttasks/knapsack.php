<?php
$value = array(60, 100, 120);
$weight = array(10, 20, 30);
$capacity = 50;
$itemsCount = 3;
$result = KnapSack($capacity, $weight, $value, $itemsCount);
echo $result;

function KnapSack($capacity, $weight, $value, $itemsCount) {
	$K = [];

	for ($i = 0; $i <= $itemsCount; ++$i)
	{
		for ($w = 0; $w <= $capacity; ++$w)
		{
			if ($i == 0 || $w == 0)
				$K[$i][$w] = 0;
			else if ($weight[$i - 1] <= $w)
				$K[$i][$w] = max($value[$i - 1] + $K[$i - 1][$w - $weight[$i - 1]], $K[$i - 1][$w]);
			else
				$K[$i][$w] = $K[$i - 1][$w];
		}
	}

	return $K[$itemsCount][$capacity];
}